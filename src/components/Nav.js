import React from 'react'
import { NavLink } from 'react-router-dom'

export default function Nav() {
  return (
    <nav className='nav'>
      <ul>
        <li>
          <NavLink to='/' exact activeClassName='active'>
            Poll
          </NavLink>
        </li>
        <li>
          <NavLink to='/viewpoll' activeClassName='active'>
            Stats
          </NavLink>
        </li>
        <li>
          <NavLink to='/add' activeClassName='active'>
            Create New Poll
          </NavLink>
        </li>
      </ul>
    </nav>
  )
}
