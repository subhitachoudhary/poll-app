import React, { Component } from 'react'
import { connect } from 'react-redux'
import 'bootstrap/dist/css/bootstrap.css';
import { FormGroup, Form, Button, Input, Label } from 'reactstrap';
import { handleAddPoll } from '../actions/polls'

class AddPoll extends Component {
  state = {
    question: '',
    a: '',
    b: '',
    c: '',
    d: '',
  }
  handleInputChange = (e) => {
    const { value, name } = e.target

    this.setState(() => ({
      [name]: value
    }))
  }
  isDisabled = () => {
    const { question, a, b, c, d } = this.state

    return question === ''
      || a === ''
      || b === ''
      || c === ''
      || d === ''
  }
  handleSubmit = (e) => {
    e.preventDefault()
    this.props.history.push('/')
    this.props.dispatch(handleAddPoll(this.state))
  }
  render() {
    const { question, a, b, c, d } = this.state

    return (
      <Form onSubmit={this.handleSubmit}>
      <FormGroup>
        <Label for="exampleQuestion">What is your question?</Label>
        <Input
         id="exampleQuestion"
         value={question}
          onChange={this.handleInputChange}
          name='question'
          type='text'
           />
      </FormGroup>
      <h5>What are the options?</h5>
      <FormGroup>
        <Label for="a">Option A</Label>
        <Input
         id="a"
         value={a}
          onChange={this.handleInputChange}
          name='a'
          type='text'
           />
      </FormGroup>
      <FormGroup>
        <Label for="b">Option B</Label>
        <Input
         id="b"
         value={b}
          onChange={this.handleInputChange}
          name='b'
          type='text'
           />
      </FormGroup>
      <FormGroup>
        <Label for="c">Option C</Label>
        <Input
         id="c"
         value={c}
          onChange={this.handleInputChange}
          name='c'
          type='text'
           />
      </FormGroup>
      <FormGroup>
        <Label for="d">Option D</Label>
        <Input
         id="d"
         value={d}
          onChange={this.handleInputChange}
          name='d'
          type='text'
           />
      </FormGroup>
      <Button color="primary" type='submit' disabled={this.isDisabled()}>Submit</Button>
      </Form>


      

    )
  }
}

export default connect()(AddPoll)
